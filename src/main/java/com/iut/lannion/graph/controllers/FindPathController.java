package com.iut.lannion.graph.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.iut.lannion.graph.model.Edge;
import com.iut.lannion.graph.model.Graph;
import com.iut.lannion.graph.model.Vertex;
import com.iut.lannion.graph.routing.DijkstraPathFinder;

@RestController
public class FindPathController {

	@Autowired
	private Graph graph;

	@GetMapping(value = "/find-path")
	public List<Edge> findPath(
		@RequestParam(value = "origin", required = true)
		String originId,
		@RequestParam(value = "destination", required = true)
		String destinationId
	) {
		DijkstraPathFinder pathFinder = new DijkstraPathFinder(graph);
		Vertex origin = graph.findVertex(originId);
		Vertex destination = graph.findVertex(destinationId);
		return pathFinder.findPath(origin, destination);
	}

}
